# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt autocd beep
bindkey -v
# End of lines configured by zsh-newuser-install

### EXPORT
#export EDITOR="emacsclient -t -a ''"              # $EDITOR use Emacs in terminal
#export VISUAL="emacsclient -c -a emacs"   



#Setting up ZSH-AUTOCOMPLETE
#source /home/ehsanc/.zshplugins/zsh-autocomplete/zsh-autocomplete.plugin.zsh

#Setting up ZSH-AUTOSUGGESTIONS
#source /home/ehsanc/.zshplugins/zsh-autosuggestions/zsh-autosuggestions.zsh

#ZSH Syntax Highlighting
#source /home/ehsanc/.zshplugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh


# The following lines were added by compinstall
zstyle :compinstall filename '/home/ehsan/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall



#-------------------Aliases---------
#Git dotfiles backup
alias dfbu='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'

# vim and emacs
alias vim="nvim"
alias em="/usr/bin/emacs -nw"
alias emacs="emacsclient -c -a 'emacs'"
alias doomsync="~/.emacs.d/bin/doom sync"
alias doomdoctor="~/.emacs.d/bin/doom doctor"
alias doomupgrade="~/.emacs.d/bin/doom upgrade"
alias doompurge="~/.emacs.d/bin/doom purge"

#CD to blender addons folder
alias blenderconf="cd .config/blender/3.4/scripts/addons"

#----------------End of Aliases-----------
#----------------PATHS--------------------
export PATH="$PATH:$HOME/MyScripts"

export PATH="$PATH:$HOME/.local/bin"
#-------------END OF PATHS----------------

##


# Starship added line
#eval "$(starship init zsh)"



# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
# __conda_setup="$('/home/ehsanc/anaconda3/bin/conda' 'shell.zsh' 'hook' 2> /dev/null)"
# if [ $? -eq 0 ]; then
#     eval "$__conda_setup"
# else
#     if [ -f "/home/ehsanc/anaconda3/etc/profile.d/conda.sh" ]; then
#         . "/home/ehsanc/anaconda3/etc/profile.d/conda.sh"
#     else
#         export PATH="/home/ehsanc/anaconda3/bin:$PATH"
#     fi
# fi
# unset __conda_setup
# <<< conda initialize <<<
