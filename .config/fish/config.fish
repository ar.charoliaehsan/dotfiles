if status is-interactive
    # Commands to run in interactive sessions can go here
    set fish_greeting

#--------PATHS----------
set PATH ~/.emacs.d/bin $PATH
set PATH /home/ehsan/Softwares/Blender/blender $PATH

set PATH /home/ehsan/.local/bin $PATH
#--------PATHS end---------

#--------Powerline Shell---------------
#function fish_prompt
#         powerline-shell --shell bare $status
#end
#----------Powerline  Shell end-----------


#------------------VI MODE-----------------
function fish_user_key_bindings
    # Execute this once per mode that emacs bindings should be used in
    fish_default_key_bindings -M insert
    fish_vi_key_bindings --no-erase insert
end
set fish_cursor_default block
set fish_cursor_insert line
set fish_cursor_replace_one underscore
set fish_cursor_visual block
#------------------VI MODE END-----------------



#-------------alias-----------------------------------
#Git Bare Repository Stuff
alias dfbu='/usr/bin/git --git-dir=$HOME/dotfiles/ --work-tree=$HOME'
alias drblender='DRI_PRIME=1 blender'


#-------------alias End-----------------------------------



#-------Fish Theme------------------------------------------------------------------------------------------

#-------Fish Theme End ------------------------------------------------------------------------------------------------



    
end

